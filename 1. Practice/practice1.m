function globalClass
g = figure
h = axes
set(h,'position',[0.35 0.35 0.5 0.6])
axis([-10 10 -3 3])
xlabel('axis x')
ylabel('axis y')
title('Graph')

user_control_one = uicontrol(g,'style','edit',...
    'position',[40 80 140 35],...
    'fontsize',14)

user_control_two = uicontrol(g,'style','edit',...
    'position',[40 40 140 35],...
    'fontsize',14)

result = uicontrol('style','text',...
'position',[40 120 140 75],'fontsize',14)

uc2 = uicontrol('style','pushbutton',...
'position',[200 40 40 20],'fontsize',8,'string','boton',...
'callback',@operation)

    function operation(params, A, B)
        
        A = str2num(get(user_control_one,'string'))
        B = str2num(get(user_control_two,'string'))
        C = A + B
        
        if size(C) == 0
            set(result,'string','Operation failed, try again')
        else
            set(user_control_one,'string',' ')
            set(user_control_two,'string',' ')
            set(result,'string',num2str(C))
            
            if max(size(C)) == 3 
              plot3(C(1,:), C(2,:), C(3,:) )
            else
              plot(C(1,:), C(2,:))
            end
        end
        
        axis([-5 5 -7 7])
        xlabel('axis x')
        ylabel('axis y')
        grid on
        hold on  
        
    end
end
